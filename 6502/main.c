#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

enum addressing {
	A_A,
	A_ABS,
	A_ABS_X,
	A_ABS_Y,
	A_IMM,
	A_IMPL,
	A_IND,
	A_X_IND,
	A_IND_Y,
	A_REL,
	A_ZPG,
	A_ZPG_X,
	A_ZPG_Y
};

static const char* mnemonic;

/* https://www.masswerk.at/6502/6502_instruction_set.html */
enum mnemonic {
	NONE,
	ADC, /* add with carry */
	AND, /* and (with accumulator) */
	ASL, /* arithmetic shift left */
	BCC, /* branch on carry clear */
	BCS, /* branch on carry set */
	BEQ, /* branch on equal (zero set) */
	BIT, /* bit test */
	BMI, /* branch on minus (negative set) */
	BNE, /* branch on not equal (zero clear) */
	BPL, /* branch on plus (negative clear) */
	BRK, /* break / interrupt */
	BVC, /* branch on overflow clear */
	BVS, /* branch on overflow set */
	CLC, /* clear carry */
	CLD, /* clear decimal */
	CLI, /* clear interrupt disable */
	CLV, /* clear overflow */
	CMP, /* compare (with accumulator) */
	CPX, /* compare with X */
	CPY, /* compare with Y */
	DEC, /* decrement */
	DEX, /* decrement X */
	DEY, /* decrement Y */
	EOR, /* exclusive or (with accumulator) */
	INC, /* increment */
	INX, /* increment X */
	INY, /* increment Y */
	JMP, /* jump */
	JSR, /* jump subroutine */
	LDA, /* load accumulator */
	LDX, /* load X */
	LDY, /* load Y */
	LSR, /* logical shift right */
	NOP, /* no operation */
	ORA, /* or with accumulator */
	PHA, /* push accumulator */
	PHP, /* push processor status (SR) */
	PLA, /* pull accumulator */
	PLP, /* pull processor status (SR) */
	ROL, /* rotate left */
	ROR, /* rotate right */
	RTI, /* return from interrupt */
	RTS, /* return from subroutine */
	SBC, /* subtract with carry */
	SEC, /* set carry */
	SED, /* set decimal */
	SEI, /* set interrupt disable */
	STA, /* store accumulator */
	STX, /* store X */
	STY, /* store Y */
	TAX, /* transfer accumulator to X */
	TAY, /* transfer accumulator to Y */
	TSX, /* transfer stack pointer to X */
	TXA, /* transfer X to accumulator */
	TXS, /* transfer X to stack pointer */
	TYA, /* transfer Y to accumulator */
};

static const char* opnames[] = { "ERROR, MY SHIT DON'T WORK!",
	"ADC", "AND", "ASL", "BCC", "BCS", "BEQ", "BIT", "BMI", "BNE", "BPL",
	"BRK", "BVC", "BVS", "CLC", "CLD", "CLI", "CLV", "CMP", "CPX", "CPY",
	"DEC", "DEX", "DEY", "EOR", "INC", "INX", "INY", "JMP", "JSR", "LDA",
	"LDX", "LDY", "LSR", "NOP", "ORA", "PHA", "PHP", "PLA", "PLP", "ROL",
	"ROR", "RTI", "RTS", "SBC", "SEC", "SED", "SEI", "STA", "STX", "STY",
	"TAX", "TAY", "TSX", "TXA", "TXS", "TYA"
};

struct instruction {
	enum mnemonic name;
	enum addressing addressing;
};

/* perl -pe '
	s/\bA\b/A_A/g;s/abs,X/A_ABS_X/g;s/abs,Y/A_ABS_Y/g;s/abs/A_ABS/g;
	s/impl/A_IMPL/g;s/X,ind/A_X_IND/g;s/ind,Y/A_IND_Y/g;s/ind/A_IND/g;
	s/#/A_IMM/g;s/rel/A_REL/g;s/zpg,X/A_ZPG_X/g;s/zpg,Y/A_ZPG_Y/g;
	s/zpg/A_ZPG/g;s/\t/} {/g;s/ /,/g;s/---/NONE, A_A/g;
	chomp;print "{$_},\n";
	' tbl.txt
*/
struct instruction op_table[] = {
	{BRK,A_IMPL},{ORA,A_X_IND},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ORA,A_ZPG},{ASL,A_ZPG},{NONE, A_A},{PHP,A_IMPL},{ORA,A_IMM},{ASL,A_A},{NONE, A_A},{NONE, A_A},{ORA,A_ABS},{ASL,A_ABS},{NONE, A_A},
	{BPL,A_REL},{ORA,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ORA,A_ZPG_X},{ASL,A_ZPG_X},{NONE, A_A},{CLC,A_IMPL},{ORA,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ORA,A_ABS_X},{ASL,A_ABS_X},{NONE, A_A},
	{JSR,A_ABS},{AND,A_X_IND},{NONE, A_A},{NONE, A_A},{BIT,A_ZPG},{AND,A_ZPG},{ROL,A_ZPG},{NONE, A_A},{PLP,A_IMPL},{AND,A_IMM},{ROL,A_A},{NONE, A_A},{BIT,A_ABS},{AND,A_ABS},{ROL,A_ABS},{NONE, A_A},
	{BMI,A_REL},{AND,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{AND,A_ZPG_X},{ROL,A_ZPG_X},{NONE, A_A},{SEC,A_IMPL},{AND,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{AND,A_ABS_X},{ROL,A_ABS_X},{NONE, A_A},
	{RTI,A_IMPL},{EOR,A_X_IND},{NONE, A_A},{NONE, A_A},{NONE, A_A},{EOR,A_ZPG},{LSR,A_ZPG},{NONE, A_A},{PHA,A_IMPL},{EOR,A_IMM},{LSR,A_A},{NONE, A_A},{JMP,A_ABS},{EOR,A_ABS},{LSR,A_ABS},{NONE, A_A},
	{BVC,A_REL},{EOR,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{EOR,A_ZPG_X},{LSR,A_ZPG_X},{NONE, A_A},{CLI,A_IMPL},{EOR,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{EOR,A_ABS_X},{LSR,A_ABS_X},{NONE, A_A},
	{RTS,A_IMPL},{ADC,A_X_IND},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ADC,A_ZPG},{ROR,A_ZPG},{NONE, A_A},{PLA,A_IMPL},{ADC,A_IMM},{ROR,A_A},{NONE, A_A},{JMP,A_IND},{ADC,A_ABS},{ROR,A_ABS},{NONE, A_A},
	{BVS,A_REL},{ADC,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ADC,A_ZPG_X},{ROR,A_ZPG_X},{NONE, A_A},{SEI,A_IMPL},{ADC,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{ADC,A_ABS_X},{ROR,A_ABS_X},{NONE, A_A},
	{NONE, A_A},{STA,A_X_IND},{NONE, A_A},{NONE, A_A},{STY,A_ZPG},{STA,A_ZPG},{STX,A_ZPG},{NONE, A_A},{DEY,A_IMPL},{NONE, A_A},{TXA,A_IMPL},{NONE, A_A},{STY,A_ABS},{STA,A_ABS},{STX,A_ABS},{NONE, A_A},
	{BCC,A_REL},{STA,A_IND_Y},{NONE, A_A},{NONE, A_A},{STY,A_ZPG_X},{STA,A_ZPG_X},{STX,A_ZPG_Y},{NONE, A_A},{TYA,A_IMPL},{STA,A_ABS_Y},{TXS,A_IMPL},{NONE, A_A},{NONE, A_A},{STA,A_ABS_X},{NONE, A_A},{NONE, A_A},
	{LDY,A_IMM},{LDA,A_X_IND},{LDX,A_IMM},{NONE, A_A},{LDY,A_ZPG},{LDA,A_ZPG},{LDX,A_ZPG},{NONE, A_A},{TAY,A_IMPL},{LDA,A_IMM},{TAX,A_IMPL},{NONE, A_A},{LDY,A_ABS},{LDA,A_ABS},{LDX,A_ABS},{NONE, A_A},
	{BCS,A_REL},{LDA,A_IND_Y},{NONE, A_A},{NONE, A_A},{LDY,A_ZPG_X},{LDA,A_ZPG_X},{LDX,A_ZPG_Y},{NONE, A_A},{CLV,A_IMPL},{LDA,A_ABS_Y},{TSX,A_IMPL},{NONE, A_A},{LDY,A_ABS_X},{LDA,A_ABS_X},{LDX,A_ABS_Y},{NONE, A_A},
	{CPY,A_IMM},{CMP,A_X_IND},{NONE, A_A},{NONE, A_A},{CPY,A_ZPG},{CMP,A_ZPG},{DEC,A_ZPG},{NONE, A_A},{INY,A_IMPL},{CMP,A_IMM},{DEX,A_IMPL},{NONE, A_A},{CPY,A_ABS},{CMP,A_ABS},{DEC,A_ABS},{NONE, A_A},
	{BNE,A_REL},{CMP,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{CMP,A_ZPG_X},{DEC,A_ZPG_X},{NONE, A_A},{CLD,A_IMPL},{CMP,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{CMP,A_ABS_X},{DEC,A_ABS_X},{NONE, A_A},
	{CPX,A_IMM},{SBC,A_X_IND},{NONE, A_A},{NONE, A_A},{CPX,A_ZPG},{SBC,A_ZPG},{INC,A_ZPG},{NONE, A_A},{INX,A_IMPL},{SBC,A_IMM},{NOP,A_IMPL},{NONE, A_A},{CPX,A_ABS},{SBC,A_ABS},{INC,A_ABS},{NONE, A_A},
	{BEQ,A_REL},{SBC,A_IND_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{SBC,A_ZPG_X},{INC,A_ZPG_X},{NONE, A_A},{SED,A_IMPL},{SBC,A_ABS_Y},{NONE, A_A},{NONE, A_A},{NONE, A_A},{SBC,A_ABS_X},{INC,A_ABS_X},{NONE, A_A},
};



int ch, ch2, ch3;

static void db(int ch) {
	printf("db 0x%x", ch & 0xFF);
	if(isprint(ch)) {
		printf("\t; '%c'", ch);
	}
	putchar('\n');
}


static int emit_abs(struct instruction* i) {
	ch2 = getchar();
	if(ch2 == EOF) {
		db(ch);
		return 0;
	}

	ch3 = getchar();
	if(ch3 == EOF) {
		db(ch);
		db(ch2);
		return 0;
	}
	if(i->name == JSR || i->name == JMP) {
		printf("%s 0x%hx\n", opnames[i->name], (ch2 & 0xFF) | (((ch3 & 0xFF) << 8)));
	} else {
		printf("%s [0x%hx", opnames[i->name], (ch2 & 0xFF) | (((ch3 & 0xFF) << 8)));
	}
	return 0;
}

static int emit_zpg(struct instruction* i) {
	ch2 = getchar();
	if(ch2 == EOF) {
		db(ch);
		return 0;
	}

	printf("%s [0x%hhx", opnames[i->name], (ch2 & 0xFF));
	return 1;
}

int main() {

	while((ch = getchar()) != EOF) {
		struct instruction* i = &op_table[ch];

		if(i->name == NONE) {
			db(ch);
			continue;
		}

		switch(i->addressing) {
			case A_A:
				printf("%s A\n", opnames[i->name]);
			break;
			case A_ABS:
			case A_IND:
				if(emit_abs(i))
					puts("]" + (i->name == JSR || i->name == JMP));
			break;
			case A_ABS_X:
				if(emit_abs(i))
					puts("+X]");
			break;
			case A_ABS_Y:
				if(emit_abs(i))
					puts("+Y]");
			break;
			case A_IMM:
				ch2 = getchar();
				if(ch2 == EOF) {
					db(ch);
					return 0;
				}
				printf("%s 0x%hx\n", opnames[i->name], ch2);
			break;
			case A_IMPL:
				puts(opnames[i->name]);
			break;
			case A_X_IND:
				ch2 = getchar();
				if(ch2 == EOF) {
					db(ch);
					return 0;
				}
			
				printf("%s [[%hu + X]]\n", opnames[i->name], ch2);
			break;
			case A_IND_Y:
				ch2 = getchar();
				if(ch2 == EOF) {
					db(ch);
					return 0;
				}
			
				printf("%s [[%hu] + Y]\n", opnames[i->name], ch2);
			break;
			case A_REL:
				ch2 = getchar();
				if(ch2 == EOF) {
					db(ch);
					return 0;
				}

				signed char c = ch2;
				printf("%s [PC+%+hhd]\n", opnames[i->name], c);
			break;
			case A_ZPG:
				if(emit_zpg(i))
					puts("]");
			break;
			case A_ZPG_X:
				if(emit_zpg(i))
					puts("+X]");
			break;
			case A_ZPG_Y:
				if(emit_zpg(i))
					puts("+Y]");
			break;
		}
	}
}
